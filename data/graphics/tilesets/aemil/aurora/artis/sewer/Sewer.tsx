<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.1" name="Sewer" tilewidth="32" tileheight="32" tilecount="256" columns="16">
 <image source="sewer.png" width="512" height="512"/>
 <tile id="144">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,32 32,32 32,0"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
