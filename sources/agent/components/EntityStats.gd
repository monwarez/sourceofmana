extends Object
class_name EntityStats

var level : int							= 52
var experience : float					= 80.569999

var moveAcceleration : int				= 800
var moveFriction : int					= 1000
var moveSpeed : int						= 100

var maxHealth : int						= 80
var maxMana : int						= 50
var maxStamina : int					= 100
var maxWeight : float					= 25200
var health : int						= 40
var mana : int							= 10
var stamina : int						= 80
var weight : float						= 4.2

var attackSpeed : int					= 400
